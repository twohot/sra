"""Classes for handling Contact Information"""


class Contact:
    """A Place"""

    def __init__(self, **kwargs):
        """Initialize Place/Address Variables"""

        self.__contact = {
            "record": None,
            "owner": None,
            "type": None,
            "linkedto": None,
            "street": None,
            "area": None,
            "Village": None,
            "lga": None,
            "city": None,
            "zipcode": None,
            "PostMail": None,
            "State": None,
            "Country": None,
            "Phone": None,
            "Email": None,
        }

        for key, value in kwargs.items():
            if key in self.__contact:
                self.__contact[key] = value

    @property
    def owner(self):
        """Return the Id-number of this Contact's owner"""

        return self.__contact["owner"]

    @property
    def data(self):
        """Return tuple of Contact Data"""

        return tuple(self.__contact.values())
