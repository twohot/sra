"""Class Objects describing Roles in a School"""

from programme import Register
from records import Records
from person import Person

# LEVELS = '*YEAR|LEVEL|CLASS'


class Student(Person):
    """A Student Object"""

    def __init__(self):
        """Initialize Student"""

        super().__init__()
        self.__student = {
            "number": "",
            "sponsor": 0,
            "department": 0,
            "prog": 0,  # Name of Curriculum
            "stage": 0,
            "entrymode": 0,
            "entryset": 0,  # Registration Set
        }
        self.__register = Register()

    def refresh(self):
        """Load student's data.
        Returns True if successful, False otherwise."""

        super().__init__()
        with Records(self.number) as records:
            data = records.student()[0]
            if data:
                (
                    self.__student["department"],
                    self.__student["prog"],
                    self.__student["stage"],
                    self.__student["entrymode"],
                    self.__student["entryset"],
                ) = data[1:]
                biodata = records.studentbio()[0]
                if biodata:
                    self.name.update(datatuple=biodata[1:7])
                    self.bio.update(
                        datatuple=(tuple([biodata[0]] + list(biodata[7:11])))
                    )
                    self.religion = biodata[11]
        self.register.__init__(self.number, self.stage, self.mode)

    def save(self):
        """Write student's data"""

        print(f"Writing id={self.__student['id']} to database")

    @property
    def number(self):
        """Return Student's registration number"""

        return self.__student["number"]

    @number.setter
    def number(self, value):
        """Set Student's registration number.
        Setting this will refresh all data associated with this student"""

        if isinstance(value, str):
            self.__student["number"] = value
            self.refresh()

    @property
    def mode(self):
        """Return mode of entry"""

        return self.__student["entrymode"]

    @property
    def programme(self):
        """Return the registered program"""

        return self.__student["prog"]

    @property
    def results(self):
        """Return an Object for managing Students's Results"""

        return self.__register.results

    @property
    def register(self):
        """Return an Object for managing Registrations"""

        return self.__register

    @property
    def stage(self):
        """Return the stage of this Student"""

        return self.__student["stage"]
