"""Class Objects for SRA People"""


class Properties:
    """Manages Properties of Same Type"""

    def __init__(self, TypeClass, name=None):
        """Initialize Grouped Properties Manager"""

        self.__props = {
            "class": TypeClass,
            "name": name,
            "instances": {},
        }

    def bear(self, **kwargs):
        """Instantiate a new member of this group (Properties)
        as per Initiated Class"""

        name = kwargs.get("name", False)
        if name and not self.has(name):
            self.__props["instances"][name] = self.__props["class"](**kwargs)
            return True
        return False

    def clear(self):
        """Remove all the properties in this Group"""

        self.__props["instances"].clear()

    def get(self, name):
        """Return the named Property"""

        if name in self.__props["instances"]:
            return self.__props["instances"][name]
        return None

    def shed(self, name):
        """Let go of the property bearing the name"""

        if name in self.__props["instances"]:
            del self.__props["instances"][name]

    def members(self, name=None):
        """Return List of sub-properties/members in named property
        if name is not provided, list Keys in this Group Object"""

        if name and self.has(name):
            return list(self.__props["instances"][name].__dict__.keys())
        return list(self.__props["instances"].keys())

    @property
    def name(self):
        """Return the name of this Group of Properties"""

        return self.__props["name"]

    @name.setter
    def name(self, value):
        """Set the name of this Group of Properties"""

        if isinstance(value, str):
            self.__props["name"] = value

    def has(self, name):
        """Check that a named property has and return bool"""

        return name in self.__props["instances"]

    def count(self):
        """Return the number of member properties"""

        return len(self.members())
